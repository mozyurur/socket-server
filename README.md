--- GET endpoints (4)
/   ->renders the homepage<-

/cafe/getAll  ->returns all of the cafes<-

/user/getAll -> returns all of the users<-

/user/signup ->renders the user signup page<-


--- POST Endpoints (4)
/user/signup
({
  'params' : {
    'name' : -String-,
    'email' : -String-,
    'password' : -String-
  },
  'errors' : {
    'existing-user',
    'internal-server-error'
  },
  'returns' : -create user-
  })

/user/deleteOne
({
  'params' : {
    'email' : -String-,
    'password' : -String-
  },
  'errors' : {
    'user-not-found',
    'unvalid-parameter',
    'internal-server-error'
  },
  'returns' : -deleted user count-
  })


/user/signin
({
  'params' : {
    'email' : -String-,
    'password' : -String-
  },
  'errors' : {
    'user-not-found',
    'internal-server-error'
  },
  'returns' : -true-
  })



/user/getOne
({
  'params' : {
    'email' : -String-,
    'password' : -String-
  },
  'errors' : {
    'user-not-found',
    'unvalid-parameter',
    'internal-server-error'
  },
  'returns' : -selected user-
  })
