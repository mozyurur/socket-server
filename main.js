var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var bodyParser = require('body-parser');
var Datastore = require('nedb');
var Document = require('camo').Document;
var EmbeddedDocument = require('camo').EmbeddedDocument;
var connect = require('camo').connect;
var unirest = require('unirest');




require('socketio-auth')(io, {
  authenticate: authenticate,
  timeout: 60000
});

var uri = 'nedb://data';
var jsonParser = bodyParser.json();

// create application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false });

//the redirect uri we set when registering our application
var clientId='b3e985dd5b80422fa21437f27dbbf7fb';
var clientSecret='a0d7beed90c84eacb52d7377d1ad5228';
var redirectUri = 'http://localhost:3002/handleAuth';

var responseType = 'code';
var accessToken,userFullName,userName,userId,userProfilePhoto;

var requestToGetAccessToken = unirest("POST", "https://api.instagram.com/oauth/access_token");

requestToGetAccessToken.headers({
  "content-type": "application/x-www-form-urlencoded"
});

// set the view engine to ejs
app.set('view engine', 'ejs');

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

class Name extends Document {
    constructor() {
        super();
        this.name = {
            type : String,
            unique : true,
            required : false
        };
        this.middlename = {
            type : String,
            unique : true,
            required : false
        };
        this.surname = {
            type : String,
            unique : true,
            required : false
        };
        this.fullname = {
            type : String,
            unique : true,
            required : false
        };
    }
}

class User extends Document {
    constructor() {
        super();
        this.name = {
            type : String,
            required : true
        };
        this.email = {
            type : String,
            unique : true,
            required : true
        };
        this.password = {
          type : String,
          required : true
        };
    }
    static collectionName() {
        return 'users';
    }
}

class Cafe extends Document {
    constructor() {
        super();
        this.name = {
            type : String,
            required : true
        };
        this.username = {
            type : String,
            unique : true,
            required : true
        };
        this.admins = {
            type : Array
        };
        this.employees = {
            type : Array
        };
        this.password = {
            type : String,
            required : true
        };
        this.location = {
            type : String,
            required : false,
            unique : true
        };

    }
    static collectionName() {
        return 'cafes';
    }
}

class CafeCode extends Document {
    constructor() {
        super();
        this.code = {
            type : String,
            required : true,
            unique : true
        };
        this.cafeId = {
            type : String,
            unique : true,
            required : true
        };

    }
    static collectionName() {
        return 'cafecodes';
    }
}

class TableCode extends Document {
    constructor() {
        super();
        this.code = {
            type : String,
            required : true,
            unique : true
        };
        this.cafeId = {
            type : String,
            required : true
        };
        this.tableName = {
            type : String,
            required : true
        };

    }
    static collectionName() {
        return 'cafecodes';
    }
}

connect(uri).then(function(db) {

  Cafe.find().then(function(cafes) {
    if (cafes) {
      cafes.forEach(function(cafe) {
        app.get('/'+cafe.username, function(req, res) {
          res.render('pages/index', {
                username: cafe.username,
                clientid : clientId,
                redirectUri : redirectUri
            });
        });
      });
    }else {
      res.send("error");
    }
  });



});


//
// Those are the get endpoints
//


// //Homepage directionary
app.get('/', function(req, res){
  res.render('pages/index', {
        // drinks: 'merhaba'
    });
});

// //Homepage directionary
app.get('/cafe/getAll', function(req, res){
  connect(uri).then(function(db) {
    Cafe.find().then(function(cafe) {
      if (cafe) {
          res.send(cafe);
      }else {
        res.send("error");
      }
    });
  });
});

// //Homepage directionary
app.get('/user/getAll', function(req, res){
  connect(uri).then(function(db) {
    User.find().then(function(users) {
      if (users) {
          res.status(200).json(users);
      }else {
        res.status(204).json({
          'err' : 'no-user'
        });
      }
    });
  });
});





//Signup page
app.get('/user/signup', function(req, res){
  res.sendFile(__dirname + '/public/signup.html');
});

app.get('/auth', function(req, res){
  res.redirect('https://api.instagram.com/oauth/authorize/?client_id='+clientId+'&redirect_uri='+redirectUri+'&response_type='+responseType);
});

app.get('/handleAuth', function(req, res){
  if (req.query.code) {
    requestToGetAccessToken.form({
      "client_id": clientId,
      "client_secret": clientSecret,
      "redirect_uri": redirectUri,
      "grant_type": "authorization_code",
      "code": req.query.code
    });
    requestToGetAccessToken.end(function (response) {
      if (response.error){
        throw new Error(response.error);
      } else {
        console.log(response.body);
        accessToken=response.body.access_token;
        userFullName = response.body.user.full_name;
        userName = response.body.user.username;
        userProfilePhoto = response.body.user.profile_picture;
        res.render('pages/afterauth', {
              username: response.body.user.username,
              fullname: response.body.user.full_name,
              profilepicture: response.body.user.profile_picture
          });
      }
    });
  }else {
    console.log("Error: without parameter");
  }
});

//
//Those are the POST endpoints
//

//Signup API endpoint
app.post('/user/signup', urlencodedParser, function(req, res){
    connect(uri).then(function(db) {
      User.findOne({ email:  req.body.email }).then(function(user) {
        if (user) {
          res.status(400).json({
            'err' : 'existing-user'
          });
        }else {
          try {
            User.create({
                name : req.body.name,
                password: req.body.password,
                email: req.body.email
            }).save().then(function(user) {
              res.status(200).json(user);
            });
          } catch (e) {
            res.status(500).json({
              'err' : 'internal-server-error'
            });
          }
        }
      });
    });
});

//Delete User Endpoint
app.post('/user/deleteOne', urlencodedParser, function(req, res){
  if (req.body.email!=""&&req.body.password!="") {
    try {
      connect(uri).then(function(db) {
          User.findOne({ email:  req.body.email ,password:  req.body.password}).then(function(user) {
            if (user) {
              try {
                User.deleteOne({ email: req.body.email }).then(function(deletedusercount) {
                  res.status(200).json({
                    'msg' : deletedusercount
                  });
                });
              } catch (e) {
                res.status(400).json({
                  'err' : 'internal-server-error'
                });
              }
            }else {
              res.status(200).json({
                'err' : 'user-not-found'
              });
            }
          });
      });
    } catch (e) {
      res.status(500).json({
        'err' : 'internal-server-error'
      })
    }

  }else {
    res.status(400).json({
      'err' : 'unvalid-parameter'
    });
  }

});

//Signin request handler
app.post('/user/signin', urlencodedParser, function(req, res){
  try {
    connect(uri).then(function(db) {
      User.findOne({ email:  req.body.email , password:  req.body.password}).then(function(user) {
        if (user) {
          res.status(200).json({
            'msg' : true,
            'user' : user
          });
        }else {
          res.status(200).json({
            'err' : 'user-not-found'
          });
        }
      });
    });
  } catch (e) {
    res.status(500).json({
      'err' : 'internal-server-error'
    });
  }

});

//Get wanted user
app.post('/user/getOne', urlencodedParser, function(req, res){
  if (req.body.email&&req.body.password) {
    try {
      connect(uri).then(function(db) {
        User.findOne({ email:  req.body.email, password:  req.body.password }).then(function(user) {
          if (user) {
            res.status(200).json(user);
          }else {
            res.status(200).json({
              'err' : 'user-not-found'
            });
          }
        });
      });
    } catch (e) {
      res.status(500).json({
        'err' : 'internal-server-error'
      });
    }
  }else {
    res.status(400).json({
      'err' : 'unvalid-parameter'
    });
  }
});

// //Check if code exist
// app.post('/table/checkCode', urlencodedParser, function(req, res){
//   connect(uri).then(function(db) {
//     TableCode.findOne({ code : req.body.code}).then(function(code) {
//       if (code) {
//         res.status(200).json(code);
//       }else {
//         res.status(200).json({
//           'err' : 'code-not-found'
//         });
//       }
//     });
//   });
// });
//
// //Check if code exist
// app.post('/table/createCode', urlencodedParser, function(req, res){
//   var generatedTableCode='';
//   var isUnique=false;
//   connect(uri).then(function(db) {
//     for (var ai = 0; ai < 1; ai++) {
//       for (var i = 0; i < 4; i++) {
//         generatedTableCode += (Math.floor(Math.random() * 10)).toString();
//       }
//       console.log(generatedTableCode);
//       TableCode.findOne({ table : req.body.table}).then(function(tableCode) {
//         if (tableCode) {
//           res.status(200).json({
//             'err' : 'table-has-code'
//           });
//         }else {
//           TableCode.findOne({ code : generatedTableCode}).then(function(tableCode) {
//             if (tableCode) {
//               ai--;
//             }else {
//               TableCode.create({
//                   code : generatedTableCode,
//                   table : req.body.table
//               }).save().then(function(tableCode) {
//                 res.status(200).json(tableCode);
//               });
//
//             }
//           });
//         }
//       });
//
//     }
//   });
//
// });

//
//Those are the SOCKET LISTENERS
//


io.on("connection",function(socket){

  socket.on('message',function(arg){
    var roomName = (socket.rooms[Object.keys(socket.rooms)[1]]).split('-');
    io.to(roomName[0]+'-desktop').emit('message',arg);
    socket.emit('message','Sent Message : '+arg);
  });

  socket.on('get-cafes', function () {
    connect(uri).then(function(db) {

      Cafe.find().then(function(cafes) {
        if (cafes) {
          var cafesList = [];
          cafs.forEach(function(cafe) {
            cafesList.push(cafe.username);
          });
          socket.emit('cafes-got',cafesList);
        }else {
          res.send("error");
         }
      });
    });
  });

  socket.on('disconnect', function () {
        console.log('Socket connection ended');
  });

});




//
//Those are the functions
//

function authenticate(socket, data, callback) {
  if (data.password) {
    var email = data.email;
    var password = data.password;
    connect(uri).then(function(db) {
      User.findOne({ email : email }).then(function(user) {
        if (user&&password==user.password) {
          socket.join(email+'-desktop');
          console.log('Socket connection joined '+username+'-desktop');
          return callback(null, true);
        }else {
          return callback(new Error("User not found"));
        }
      });
    });
  }else if (data.username) {
    var username = data.username;
    connect(uri).then(function(db) {
      User.findOne({ username : username }).then(function(user) {
        if (user) {
          for (var i = 0; i < Object.keys(socket.rooms).length; i++) {
            if (i!=0) {
              socket.leave(Object.keys(socket.rooms)[i]);
            }
          }

          socket.join(username+'-client');
          console.log('Socket connection joined '+username+'-client');
          return callback(null, true);
        }else {
          return callback(null, false);
        }


      });
    });
  }else {
      return callback(null, false);
  }



}


// Async function
function getRecentMedia(accesstoken) {
  return new Promise(resolve => {

    var requestToGetRecentPosts = unirest("GET", "http://api.instagram.com/v1/users/self/media/recent/");

    requestToGetRecentPosts.query({
      "access_token": accesstoken,
      "count":10
    });


    requestToGetRecentPosts.end(function (response) {
      if (response.error){
        resolve(response.error);
      } else {
        resolve(response.body);
      }
    });

  });
}

function generateCode(){
  return new Promise(resolve => {

  });
}


http.listen(3002, function(){
  console.log('listening on *:3002');
});
