'use strict';

var Document = require('camo').Document;


module.exports = class User extends Document {
      constructor() {
          super();
          this.name = String;
          this.username = {
              type : String,
              unique : true
          };
          this.email = {
              type : String,
              unique : true,
              required : true
          };
          this.password = {
            type : String,
            required : true
          };
          this.access_permission = {
            type : Boolean,
            required : true
          };
          this.type = {
            type : String,
            required : true
          };
          this.locations = {
            type : Array,
            required : false
          };
          this.tags = {
            type : Array,
            required : false
          };
      }
      static collectionName() {
          return 'users2';
      }
}
